package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import entity.Employee;
import util.HibernateUtil;



@WebServlet(name = "BaseServlet", urlPatterns = {"/"})
public class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BaseServlet() {
        super();
    }

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Employee> employees = new ArrayList<Employee>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			employees = session.createQuery("from Employee").list();
			tx.commit();
		}catch (Exception e) {
			if (tx != null) {
                tx.rollback();
            }
			e.printStackTrace();
		}
		
		request.setAttribute("pageTitle", "Home Page");
		request.setAttribute("helloMsg", "Hello !");
		request.setAttribute("employees", employees);
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/index.jsp");
		view.forward(request, response);

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
