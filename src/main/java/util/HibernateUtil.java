package util;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	 private static Configuration configuration;
	 private static SessionFactory sessionFactory;
	
	 static {
		 try {
			 configuration = new Configuration().configure("config/hibernate.cfg.xml");
			 sessionFactory = configuration.buildSessionFactory();
		 }catch (Throwable ex) {
			 System.err.println("Initial SessionFactory creation failed." + ex);
	         throw new ExceptionInInitializerError(ex);
		}
	 }
	
	 public static Session openSession() {
	  Session session = null;
	  if (sessionFactory != null) {
	   session = sessionFactory.openSession();
	  }
	  return session;
	 }
}